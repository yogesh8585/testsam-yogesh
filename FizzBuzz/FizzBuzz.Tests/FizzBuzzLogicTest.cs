﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FizzBuzz.Model;

namespace FizzBuzz.Tests
{
    [TestFixture]
    public class FizzBuzzLogicTest
    {
        [Test]
        public void Input_Number_24_Should_Have_24_NumbersCount()
        {
            IStrategic StrategicLogic = new FizzBuzzStrategic();
            List<IFizzBuzz> list = StrategicLogic.GetList(24);
            Assert.AreEqual(24, list.Count);
            Assert.AreEqual("1", ((IFizzBuzz)list[0]).Message);
            Assert.AreEqual("2", ((IFizzBuzz)list[1]).Message);
        }

        [Test]
        public void Input_Number_Replace_3_Divisible_With_Proper_Message_Color()
        {
            IStrategic StrategicLogic = new FizzBuzzStrategic();
            List<IFizzBuzz> list = StrategicLogic.GetList(24);
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wizz", ((IFizzBuzz)list[2]).Message);
                Assert.AreEqual("Wizz", ((IFizzBuzz)list[5]).Message);
            }
            else
            {
                Assert.AreEqual("Fizz", ((IFizzBuzz)list[2]).Message);
                Assert.AreEqual("Fizz", ((IFizzBuzz)list[5]).Message);
            }
            Assert.AreEqual("blue", ((IFizzBuzz)list[2]).MessageColor);
            Assert.AreEqual("blue", ((IFizzBuzz)list[5]).MessageColor);
        }

        [Test]
        public void Input_Number_Replace_5_Divisible_With_Proper_Message_Color()
        {
            IStrategic StrategicLogic = new FizzBuzzStrategic();
            List<IFizzBuzz> list = StrategicLogic.GetList(24);
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wuzz", ((IFizzBuzz)list[4]).Message);
                Assert.AreEqual("Wuzz", ((IFizzBuzz)list[9]).Message);
            }
            else
            {
                Assert.AreEqual("Buzz", ((IFizzBuzz)list[4]).Message);
                Assert.AreEqual("Buzz", ((IFizzBuzz)list[9]).Message);
            }
            Assert.AreEqual("green", ((IFizzBuzz)list[4]).MessageColor);
            Assert.AreEqual("green", ((IFizzBuzz)list[9]).MessageColor);
        }

        [Test]
        public void Input_Number_Replace_3_and_5_Divisible_With_Proper_Message()
        {
            IStrategic StrategicLogic = new FizzBuzzStrategic();
            List<IFizzBuzz> list = StrategicLogic.GetList(24);
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wizz Wuzz", ((IFizzBuzz)list[14]).Message);
            }
            else
            {
                Assert.AreEqual("Fizz Buzz", ((IFizzBuzz)list[14]).Message);
            }
        }
    }
}
