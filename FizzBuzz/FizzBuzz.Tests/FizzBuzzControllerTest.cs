﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using FizzBuzz.Model;
using System.Web.Mvc;
using FizzBuzz.Controllers;
using FizzBuzz.Models;

namespace FizzBuzz.Tests
{
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        [Test]
        public void Should_Give_FizzBuzzView_On_InputRequest()
        {
            Mock<IStrategic> StrategicLogic = new Mock<IStrategic>();
            FizzBuzzController controllerToTest = new FizzBuzzController(StrategicLogic.Object);
            var resultView = controllerToTest.FizzBuzzView() as ViewResult;
            Assert.AreEqual("FizzBuzzView", resultView.ViewName);
        }

        [Test]
        public void Should_Show__20_Items_On_One_Page()
        {
            Mock<IStrategic> StrategicLogic = new Mock<IStrategic>();
            StrategicLogic.Setup(m => m.GetList(21)).Returns(getFizzBuzzList(21));
            FizzBuzzController controller = new FizzBuzzController(StrategicLogic.Object);
            var result = controller.FizzBuzzView(new FizzBuzzModel { Number = 21 }) as ViewResult;
            var viewModel = (FizzBuzzModel)result.Model;
            Assert.AreEqual(20, viewModel.DivisionList.Count);
        }

        [Test]
        public void Should_implement_PageCount()
        {
            Mock<IStrategic> StrategicLogic = new Mock<IStrategic>();
            StrategicLogic.Setup(m => m.GetList(63)).Returns(getFizzBuzzList(63));
            FizzBuzzController controller = new FizzBuzzController(StrategicLogic.Object);
            var result = controller.FizzBuzzView(new FizzBuzzModel { Number = 63 }) as ViewResult;
            var viewModel = (FizzBuzzModel)result.Model;
            Assert.AreEqual(4, viewModel.Page.PageCount);
        }

        [Test]
        public void Should_implement_paging()
        {
            Mock<IStrategic> StrategicLogic = new Mock<IStrategic>();
            StrategicLogic.Setup(m => m.GetList(21)).Returns(getFizzBuzzList(21));
            FizzBuzzController controller = new FizzBuzzController(StrategicLogic.Object);
            var result = controller.FizzBuzzViewNext(new FizzBuzzModel { Page = new Paging() { PageIndex = 2 }, Number = 21 }) as ViewResult;
            var viewModel = (FizzBuzzModel)result.Model;
            Assert.AreEqual(1, viewModel.DivisionList.Count); //Second page shouold have ony 1 Value
        }

        private List<IFizzBuzz> getFizzBuzzList(int rows)
        {
            List<IFizzBuzz> list = new List<IFizzBuzz>();
            for (int index = 1; index <= rows; index++)
            {
                SimpleDivision objData = new SimpleDivision();
                objData.IsDivisible(index);
                list.Add(objData);
            }
            return list;
        }
    }
}
