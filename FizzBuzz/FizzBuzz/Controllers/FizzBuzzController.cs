﻿using FizzBuzz.Model;
using FizzBuzz.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FizzBuzz.Models;

namespace FizzBuzz.Controllers
{
    public class FizzBuzzController : Controller
    {
        private readonly IStrategic businessLogic;

        /// <summary>
        /// Controller. Creates an instance of FizzBuzzController
        /// </summary>
        public FizzBuzzController(IStrategic businessLogic)
        {
            this.businessLogic = businessLogic;
        }

        /// <summary>
        /// Action method to render the initial view.
        /// </summary>
        /// <returns>FizzBuzzView View</returns>
        [HttpGet]
        public ActionResult FizzBuzzView()
        {
            return View("FizzBuzzView", new FizzBuzzModel());
        }


        /// <summary>
        /// Action method to render on Get Data button submit.
        /// </summary>
        /// <returns>FizzBuzzView View</returns>
        [HttpPost]
        [Button(ButtonName = "DisplayList")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzView(FizzBuzzModel PostModel)
        {
            FizzBuzzModel objData = new FizzBuzzModel() { Number = PostModel.Number };
            objData.Page.PageIndex = 1;

            if (ModelState.IsValid) { fillNumberList(objData); }
            TempData["FizzBuzzPage"] = objData.Page;
            return View("FizzBuzzView", objData);
        }


        /// <summary>
        /// Action method to render on Previous button click.
        /// </summary>
        /// <returns>FizzBuzzView View</returns>
        [HttpPost]
        [Button(ButtonName = "Previous")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzViewPrevious(FizzBuzzModel PostModel)
        {
            FizzBuzzModel objData = new FizzBuzzModel() { Number = PostModel.Number };
            objData.Page = (TempData["FizzBuzzPage"] != null) ? TempData["FizzBuzzPage"] as Paging : new Paging();
            objData.Page.PageIndex = (objData.Page.PageIndex == 1) ? objData.Page.PageIndex : (objData.Page.PageIndex - 1);

            if (ModelState.IsValid) { fillNumberList(objData); }
            TempData["FizzBuzzPage"] = objData.Page;
            return View("FizzBuzzView", objData);
        }

        /// <summary>
        /// Action method to render on Next button click.
        /// </summary>
        /// <returns>FizzBuzzView View</returns>
        [HttpPost]
        [Button(ButtonName = "Next")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzViewNext(FizzBuzzModel PostModel)
        {
            FizzBuzzModel objData = new FizzBuzzModel() { Number = PostModel.Number };
            objData.Page = (TempData["FizzBuzzPage"] != null) ? TempData["FizzBuzzPage"] as Paging : new Paging();
            objData.Page.PageIndex = (objData.Page.PageIndex == objData.Page.PageCount) ? objData.Page.PageIndex : (objData.Page.PageIndex + 1);

            if (ModelState.IsValid) { fillNumberList(objData); }
            TempData["FizzBuzzPage"] = objData.Page;
            return View("FizzBuzzView", objData);
        }



        private void fillNumberList(FizzBuzzModel objData)
        {
            objData.DivisionList = businessLogic.GetList(objData.Number.Value);
            var startndex = (objData.Page.PageIndex - 1) * FizzBuzzModel.PageSize;
            objData.DivisionList = objData.DivisionList.GetRange(startndex, objData.Number.Value - startndex < FizzBuzzModel.PageSize ? objData.Number.Value - startndex : FizzBuzzModel.PageSize);
        }

    }
}
