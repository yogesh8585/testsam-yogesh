﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using FizzBuzz.Model;


namespace FizzBuzz.Models
{
    /// <summary>
    /// The view model for the Fizz Buzz Operation
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Default value of Page Size
        /// </summary>
        public const int PageSize = 20;

        public FizzBuzzModel()
        {
            Page = new Paging();
        }

        /// <summary>
        /// Get or set Page to view Paging on ViewPage
        /// </summary>
        public Paging Page { get; set; }

        int? nNumber;
        /// <summary>
        /// Gets or sets the input number.
        /// </summary>
        [Required]
        [Display(Name = "Please enter the number")]
        [Range(1, 1000, ErrorMessage = "Please enter a value between 1 and 1000")]
        public int? Number
        {
            get
            {
                return nNumber;
            }
            set
            {
                nNumber = value;
                Page.PageIndex = 1;
                Page.PageCount = (Convert.ToInt32(nNumber) / PageSize);
                Page.PageCount = ((nNumber % PageSize)) == 0 ? Page.PageCount : Page.PageCount + 1;
            }
        }

        /// <summary>
        /// Gets or sets the Division object list.
        /// </summary>
        public List<IFizzBuzz> DivisionList { get; set; }


    }

    /// <summary>
    /// Paging for have current pageIndex and PageCount
    /// </summary>
    public class Paging
    {
        /// <summary>
        /// Set Default value in constructure
        /// </summary>
        public Paging() { PageIndex = 1; }

        /// <summary>
        /// Page index of the pagination.
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// Page count of the pagination.
        /// </summary>
        public int PageCount { get; set; }
    }
}