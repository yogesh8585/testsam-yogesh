﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Model
{
    public class SimpleDivision : IFizzBuzz
    {
        public bool IsDivisible(int Number)
        {
            number = Number;
            return true;
        }

        string sMessage;
        public string Message
        {
            get { return sMessage; }
        }

        public string MessageColor
        {
            get { return string.Empty; }
        }

        int number;
        public void SetMessage(bool IsSpecificDay)
        {
            sMessage = number.ToString();
        }
    }

    public class Fizz : IFizzBuzz
    {
        public bool IsDivisible(int Number)
        {
            return (Number % 3) == 0 ? true : false;
        }

        string sMessage;
        public string Message
        {
            get { return sMessage; }
        }

        public string MessageColor
        {
            get { return "blue"; }
        }

        public void SetMessage(bool IsSpecificDay)
        {
            sMessage = IsSpecificDay ? "Wizz" : "Fizz";
        }
    }

    public class Buzz : IFizzBuzz
    {
        public bool IsDivisible(int Number)
        {
            return (Number % 5) == 0 ? true : false;
        }

        string sMessage;
        public string Message
        {
            get { return sMessage; }
        }

        public string MessageColor
        {
            get { return "green"; }
        }

        public void SetMessage(bool IsSpecificDay)
        {
            sMessage = IsSpecificDay ? "Wuzz" : "Buzz";
        }
    }

    public class FizzBuzz : IFizzBuzz
    {
        public bool IsDivisible(int Number)
        {
            return ((Number % 3) == 0 && (Number % 5) == 0) ? true : false;
        }

        string sMessage;
        public string Message
        {
            get { return sMessage; }
        }

        public string MessageColor
        {
            get { return string.Empty; }
        }

        public void SetMessage(bool IsSpecificDay)
        {
            sMessage = IsSpecificDay ? "Wizz Wuzz" : "Fizz Buzz";
        }
    }
}
