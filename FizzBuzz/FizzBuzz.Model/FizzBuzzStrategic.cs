﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Model
{

    /// <summary>
    /// The business logic of the application
    /// </summary>
    public class FizzBuzzStrategic : IStrategic
    {
        /// <summary>
        /// Gets the list of numbers from one upto the number given.
        /// </summary>
        /// <param name="number">The input number.</param>
        /// <returns>List of numbers</returns>
        public List<IFizzBuzz> GetList(int number)
        {
            List<IFizzBuzz> list = new List<IFizzBuzz>();
            for (var counter = 1; counter <= number; counter++)
            {
                IFizzBuzz obj = InstanceFactory.GetInstance(IsSpecificDay(), counter);
                if (obj != null)
                {
                    list.Add(obj);
                }
            }

            return list;
        }

        public bool IsSpecificDay()
        {
            return DateTime.Today.DayOfWeek == DayOfWeek.Wednesday ? true : false;
        }
    }

    public class InstanceFactory
    {
        static string clsnamespace = "FizzBuzz.Model.";
        static string[] TypeArry = { clsnamespace + "FizzBuzz", clsnamespace + "Fizz", clsnamespace + "Buzz", clsnamespace + "SimpleDivision" };

        public static IFizzBuzz GetInstance(bool IsSpecificDay, int number)
        {
            IFizzBuzz value = new SimpleDivision();
            foreach (string type in TypeArry)
            {
                Type objType = Type.GetType(type);
                value = (IFizzBuzz)Activator.CreateInstance(objType);
                if (value.IsDivisible(number))
                {
                    value.SetMessage(IsSpecificDay);
                    break;
                }
                else
                {
                    value = null;
                }
            }
            return value;
        }
    }
}
